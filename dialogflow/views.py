from django.shortcuts import render 
from django.http import HttpResponse, JsonResponse 
from django.views.decorators.csrf import csrf_exempt 
import json 
# define home function 
def home(request): 
    return HttpResponse('Hello World!') 
@csrf_exempt 
def webhook(request): 

    predictInput = []

    req = json.loads(request.body) 
    action = req.get('queryResult').get('action') 

    print(req)

    fulfillmentText = req['queryResult']['fulfillmentText']
    
    return JsonResponse(fulfillmentText, safe=False)